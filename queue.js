let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    collection[collection.length] = element;
    return collection;

}

function dequeue() {
    // In here you are going to remove the last element in the array
    let newCollection = [];
    for (let count = (collection.length-1); count > 0; count-- ){
        newCollection[(count-1)] = collection[count]
    }
    collection = newCollection;
    return collection;

}

function front() {
    // In here, you are going to remove the first element
    let firstElement = "";
    for( let count = 0; count < collection.length; count++ ){
        if (count == 0 ){
            firstElement = collection[count];
        }
        else{
           continue;
        }
    }
        return firstElement;
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements 
     let index = 0;

     for (let count = 0 ; count <= index ; count++){
        if (collection[count]) {
            index = index + 1;
        }
        else{
            break;
        }
     }   
   

     return index;

}

function isEmpty() {
    //it will check whether the function is empty or not

    let index = 0;

     for (let count = 0 ; count <= index ; count++){
        if (collection[count]) {
            index = index + 1
        }
        else{
            break;
        }
     }   
     
    if (index === 0){
        return true;
     }
    else{
        return false;
    }

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};